<?php
/**
 * @file
 * Custom Views field operation handler subclass.
 */

/**
 * Class apachesolr_vbo_handler_field_operations
 */
class apachesolr_vbo_handler_field_operations extends views_bulk_operations_handler_field_operations {

  /**
   * Override parent::get_entity_type().
   */
  public function get_entity_type() {
    return $this->options['vbo_settings']['apachesolr_vbo_entity_type'];
  }

  /**
   * Override parent::options_form().
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $entity_options = array();
    foreach (entity_get_info() as $machine_name => $entity_info) {
      $entity_options[$machine_name] = $entity_info['label'];
    }

    $form['vbo_settings']['apachesolr_vbo_entity_type'] = array(
      '#type' => 'select',
      '#options' => $entity_options,
      '#title' => t('Entity type mapping'),
      '#default_value' => $this->options['vbo_settings']['apachesolr_vbo_entity_type'],
      '#description' => t('The entity type the solr index should be mapped.'),
    );
  }

  /**
   * Override parent::option_definition().
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['vbo_settings']['contains']['apachesolr_vbo_entity_type'] = array(
      'default' => 'node',
    );
    return $options;
  }

  /**
   * Override parent::get_value().
   */
  public function get_value($values, $field = NULL) {
    if (strpos($this->table, 'apachesolr__') !== 0) {
      return parent::get_value($values, $field);
    }
    else {
      return $values->entity_id;
    }
  }

}
