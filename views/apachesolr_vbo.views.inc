<?php
/**
 * @file
 * Default views starting point.
 */

/**
 * Implements hook_views_data_alter().
 */
function apachesolr_vbo_views_data_alter(&$data) {
  foreach (apachesolr_load_all_environments() as $env_id => $environment) {
    $apachesolr_base_table = 'apachesolr__' . $env_id;

    $data[$apachesolr_base_table]['views_bulk_operations'] = array(
      'title' => t('Bulk operations on indexed entities'),
      'group' => t('Bulk operations'),
      'help' => t('Provide a checkbox to select the row for bulk operations.'),
      'real field' => 'entity_id',
      'field' => array(
        'handler' => 'apachesolr_vbo_handler_field_operations',
        'click sortable' => FALSE,
      ),
    );
  }
}
